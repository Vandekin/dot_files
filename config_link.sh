#!/bin/sh

##########################################
#####    To Read    ######################
##########################################

# This script is supposed to be executed at the root of your home directory

SCRIPT_NAME="config_link.sh"
SCRIPT_DIR=$(pwd)
CONFIG_DIR_PWD=$SCRIPT_DIR/../.config
CONFIG_DIR_NAME=.config
HOSTNAME=$(cat /etc/hostname)

FOLDERS=$(find .config -maxdepth 1 -type d | sed -e "1d")

HOME_FILES=$(find . -maxdepth 1 -type f | sed -e "/$SCRIPT_NAME/d" | sed -e 's%./%%' | sed -e '/fonts/d')

link() {
    ln -sf $1 $2
}

link_config() {
	cd $CONFIG_DIR_PWD
	current_folder=$(echo $1 | sed -e "s%$CONFIG_DIR_NAME/%%")
	if [ ! -d $current_folder ]
	then
		mkdir -p $current_folder
	fi
	cd $SCRIPT_DIR
	config_file=$(ls $CONFIG_DIR_NAME/$current_folder)
	#echo $config_file
    echo "->$current_folder"
	case "$current_folder" in
		"i3")
            case "$HOSTNAME" in
                "archtitude") 
                    file=$(ls -1 $CONFIG_DIR_NAME/$current_folder/ | grep laptop)
                    ln -sf $SCRIPT_DIR/$CONFIG_DIR_NAME/$current_folder/$file $CONFIG_DIR_PWD/$current_folder/${file%.laptop}
                    ;;
                "optarch" | "tharchpad")
                    file=$(ls -1 $CONFIG_DIR_NAME/$current_folder/ | grep $HOSTNAME)
                    ln -sf $SCRIPT_DIR/$CONFIG_DIR_NAME/$current_folder/$file $CONFIG_DIR_PWD/$current_folder/${file%.$HOSTNAME}
                    ;;
                *)
                    ;;
            esac
			;;
        "i3blocks")
            case "$HOSTNAME" in
                "archtitude")
                    file=$(ls -1 $CONFIG_DIR_NAME/$current_folder/ | grep $HOSTNAME)
                    ln -sf $SCRIPT_DIR/$CONFIG_DIR_NAME/$current_folder/$file $CONFIG_DIR_PWD/$current_folder/${file%.$HOSTNAME}
                    ;;
                "tharchpad")
                    file=$(ls -1 $CONFIG_DIR_NAME/$current_folder/ | grep $HOSTNAME)
                    ln -sf $SCRIPT_DIR/$CONFIG_DIR_NAME/$current_folder/$file $CONFIG_DIR_PWD/$current_folder/${file%.$HOSTNAME}
                    ;;
                *)
                    ;;
            esac
            ;;
        "compton")
            file=$(ls -1 $CONFIG_DIR_NAME/$current_folder/)
            ln -sf $SCRIPT_DIR/$CONFIG_DIR_NAME/$current_folder/$file $CONFIG_DIR_PWD/$current_folder/$file
            ;;
        "keyboard_layout")
            for file in $(ls -1 $CONFIG_DIR_NAME/$current_folder/ | sed -e 's/dist//')
            do
            ln -sf $SCRIPT_DIR/$CONFIG_DIR_NAME/$current_folder/$file $CONFIG_DIR_PWD/$current_folder/$file
            done
            ;;
        *)
            ;;
    esac
}

link_home() {
    current=$SCRIPT_DIR/../$1
    if [ -f $current ]
    then
        echo "File $1 present"
    else
        echo "File $1 non present"
        ln -sf $SCRIPT_DIR/$1 $SCRIPT_DIR/../$1
    fi
}

if [ ! -d $CONFIG_DIR_PWD ]
then
    mkdir -p $CONFIG_DIR_PWD
fi

for folder in $FOLDERS
do
    link_config $folder
done

for home_file in $HOME_FILES
do
    link_home $home_file
done



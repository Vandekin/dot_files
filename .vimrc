"" Coloration syntaxique
syn on
set syntax=on
filetype indent plugin on
set encoding=UTF-8

"" Desativation du mode de compatibilité
set nocp

"" Numéros de ligne
set nu

"" Parenthèses correspondantes
set showmatch

"" Taille de tabulation
set tabstop =4
set shiftwidth =4
set softtabstop =4
set expandtab

"" affichage des résultat de la recherche pendant la recherche
set incsearch

"" ignore la case mais pas totalement (si /test la case est ignorée)
set ignorecase
set smartcase

"" complétion
set wildmenu
set wildmode =list:longest,list:full
set wildignore =*.0,*.r,*.so,*.sl,*.tar*,*.tgz

"" Ligne du curseur
set cursorline

"" Complétion intelligente
imap <C-Space> <C-X><C-O>
set completeopt+=preview

set omnifunc=syntaxcomplete#Complete
let mapleader = ","

"" jump to the first mark of the file
map <leader><leader> gg/<++><CR>4xi

""########### autocmd for c type language ##################
"" Preprocessor
"autocmd FileType c,h,cpp,hpp,java iab #i< #include <<++>>
"autocmd FileType c,h,cpp,hpp,java iab #i" #include "<++>"
""" Conditional structures
"autocmd FileType c,h,cpp,hpp,java iab main int main (int argc, char** argv) {<CR><++><CR>}
"autocmd FileType c,h,cpp,hpp,java iab if if (<++>) {<CR><++><CR>}
"autocmd FileType c,h,cpp,hpp,java iab elif else if (<++>) {<CR><++><CR>}
"autocmd FileType c,h,cpp,hpp,java iab else else {<CR><++><CR>}
""" Loop structures
"autocmd FileType c,h,cpp,hpp,java iab while while (<++>) {<CR><++><CR>}
"autocmd FileType c,h,cpp,hpp,java iab for for (<++>; <++>; <++>) {<CR><++><CR>}



""Sauvegarde du fichier courant
map <F6> :w<CR>
inoremap <F6> <Esc>:w<CR>
vnoremap <F6> <Esc>:w<CR>!
"" Sauvegarde et fermeture de tous les fichiers
map <F7> :wqa<CR>
inoremap <F7> <Esc>:wqa<CR>
vnoremap <F7> <Esc>:wqa<CR>
"" TODO : Fermeture des tous les fichiers sans sauvegarde
map <F8> :qa<CR>
inoremap <F8> <Esc>:qa<CR>
vnoremap <F8> <Esc>:qa<CR>

map <F4> :e 
map <F2> :bp<CR>
map <F3> :bn<CR>

map <C-F4> :tabe<CR>

nnoremap <PageUp> :tabprevious<CR>
nnoremap <PageDown> :tabnext<CR>

call plug#begin('~/.vim/bundle/')
Plug 'ajmwagar/vim-deus'
Plug 'octol/vim-cpp-enhanced-highlight'
Plug 'preservim/nerdtree'
Plug 'vim-airline/vim-airline'
Plug 'nathanaelkane/vim-indent-guides'
Plug 'valloric/youcompleteme'
Plug 'iamcco/markdown-preview.nvim', { 'do': { -> mkdp#util#install() } }
Plug 'ryanoasis/vim-devicons'
cal  plug#end()

set t_Co=256
"set termguicolors

"let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
"let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"


set background=dark    " Setting dark mode
colorscheme deus
let g:deus_termcolors=256

let g:indent_guides_enable_on_vim_startup = 1

let g:ycm_global_ycm_extra_conf = "~/.vim/.ycm_extra_conf.py"

map <C-n> :NERDTreeToggle<CR>
let g:NERDTreeDirArrowExpandable = '→'
let g:NERDTreeDirArrowCollapsible = '↓'
let NERDTreeShowHidden=1

"" Markdown preview config
" set to 1, nvim will open the preview window after entering the markdown buffer
" default: 0
let g:mkdp_auto_start = 0

" set to 1, the nvim will auto close current preview window when change
" from markdown buffer to another buffer
" default: 1
let g:mkdp_auto_close = 1

" set to 1, the vim will refresh markdown when save the buffer or
" leave from insert mode, default 0 is auto refresh markdown as you edit or
" move the cursor
" default: 0
let g:mkdp_refresh_slow = 0

" set to 1, the MarkdownPreview command can be use for all files,
" by default it can be use in markdown file
" default: 0
let g:mkdp_command_for_global = 0

" set to 1, preview server available to others in your network
" by default, the server listens on localhost (127.0.0.1)
" default: 0
let g:mkdp_open_to_the_world = 0

" use custom IP to open preview page
" useful when you work in remote vim and preview on local browser
" more detail see: https://github.com/iamcco/markdown-preview.nvim/pull/9
" default empty
let g:mkdp_open_ip = ''

" specify browser to open preview page
" default: ''
let g:mkdp_browser = ''

" set to 1, echo preview page url in command line when open preview page
" default is 0
let g:mkdp_echo_preview_url = 0

" a custom vim function name to open preview page
" this function will receive url as param
" default is empty
let g:mkdp_browserfunc = ''

" options for markdown render
" mkit: markdown-it options for render
" katex: katex options for math
" uml: markdown-it-plantuml options
" maid: mermaid options
" disable_sync_scroll: if disable sync scroll, default 0
" sync_scroll_type: 'middle', 'top' or 'relative', default value is 'middle'
"   middle: mean the cursor position alway show at the middle of the preview page
"   top: mean the vim top viewport alway show at the top of the preview page
"   relative: mean the cursor position alway show at the relative positon of the preview page
" hide_yaml_meta: if hide yaml metadata, default is 1
" sequence_diagrams: js-sequence-diagrams options
let g:mkdp_preview_options = {
    \ 'mkit': {},
    \ 'katex': {},
    \ 'uml': {},
    \ 'maid': {},
    \ 'disable_sync_scroll': 0,
    \ 'sync_scroll_type': 'middle',
    \ 'hide_yaml_meta': 1,
    \ 'sequence_diagrams': {},
    \ 'flowchart_diagrams': {}
    \ }

" use a custom markdown style must be absolute path
" like '/Users/username/markdown.css' or expand('~/markdown.css')
let g:mkdp_markdown_css = ''

" use a custom highlight style must absolute path
" like '/Users/username/highlight.css' or expand('~/highlight.css')
let g:mkdp_highlight_css = ''

" use a custom port to start server or random for empty
let g:mkdp_port = ''

" preview page title
" ${name} will be replace with the file name
let g:mkdp_page_title = '「${name}」'


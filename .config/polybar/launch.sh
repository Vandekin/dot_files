#!/bin/sh

# Terminate already running bar instances
killall -q polybar

# Wait until the processes have been shut down
while pgrep -u $UID -x polybar >/dev/null; do sleep 1; done

#for m in $(polybar --list-monitors | cut -d":" -f1); do
#	WIRELESS=$(ls /sys/class/net/ | grep ^wl | awk 'NR==1{print $1}') MONITOR=$m polybar --reload mainbar &
#done
#
#echo "Bars launched..."
#export $MAINBAR_MONITOR
#export $SECONDBAR_MONITOR


case "$1" in
    "laptop")
        MAINBAR_MONITOR="eDP-1" polybar --reload -c /home/nikola/.config/polybar/config.ini maintop &
        MAINBAR_MONITOR="eDP-1" polybar --reload -c /home/nikola/.config/polybar/config.ini media &
        ;;
    "1external")
        MAINBAR_MONITOR="HDMI-1" polybar --reload -c /home/nikola/.config/polybar/config.ini maintop &
        MAINBAR_MONITOR="HDMI-1" polybar --reload -c /home/nikola/.config/polybar/config.ini media &
        OTHERBAR_MONITOR="eDP-1" polybar --reload -c /home/nikola/.config/polybar/config.ini secondtop &
        ;;
    "2external")
        MAINBAR_MONITOR="HDMI-1" polybar --reload -c /home/nikola/.config/polybar/config.ini maintop &
        MAINBAR_MONITOR="HDMI-1" polybar --reload -c /home/nikola/.config/polybar/config.ini media &
        OTHERBAR_MONITOR="eDP-1" polybar --reload -c /home/nikola/.config/polybar/config.ini secondtop &
        OTHERBAR_MONITOR="DP-1"  polybar --reload -c /home/nikola/.config/polybar/config.ini secondtop &
        ;;
    *)
        ;;
esac

echo "Bars launched ..."

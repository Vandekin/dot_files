#
# ~/.bashrc
#
export LANG=en_US.UTF-8
export LC_MESSAGES="C"
export LC_CTYPE="en_US.UTF-8"
color_prompt=yes
export PATH=$PATH:/home/nikola/.script:/usr/bin/:/home/nikola/.gem/ruby/2.6.2/bin
export VISUAL=vim
shopt -s autocd
export LD_LIBRARY_PATH=/usr/local/lib:/usr/include/SDL
export TERMINAL=urxvt
DEFAULT=$PS1


# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
PS1='[\u@\h:\w\$]\n>'

if [ "$color_prompt" = yes ]; then
	PS1='\[\033[31m\][\[\033[33m\]\u@\h\[\033[037m\]:\[\033[1;36m\]\w\[\033[0;33m\]\$\[\033[31m\]]\[\033[37m\]\n\[\033[1;33m\]>\[\033[0;37m\] '
else
	PS1='[\u@\h \W\$]'
fi

#############
## Aliases ##
#############

## System alises ##
alias hig='history | grep'
alias ipp='curl https://doshi.re/ip/'
alias shut='shutdown -P now'
alias x='startx'
alias mount='sudo mount'
alias umount='sudo umount'
alias q='exit'
alias l='ls --group-directories-first'
alias ll='ls -lh'
alias la='ls -a'
alias rf='clear && source ~/.bashrc'
alias t='touch'
alias top='htop'
alias c='clear && neofetch && l' 
alias cat='ccat'


## Config alises ##
alias bash='vim ~/.bashrc'
alias i3='vim ~/.config/i3/config'
alias i3b='vim ~/.config/i3blocks/config'
alias vimrc='vim ~/.vimrc'


## App aliases ##
alias v='vim'
alias am='alsamixer'

#alias make='compilation_sound'

################
## End alises ##
################

## Launch at open ##
l
